Team: AJCM
Members: Michael Yen, Jianfu Situ, Jonathan Apostol, Chris Ellis

Comments: The HTML layout files can be found in the assignment_2 folder. 

The Walkiki_Summary_Document_and_Images folder contains images used in the summary document as well as the summary document in PDF and docx formats.