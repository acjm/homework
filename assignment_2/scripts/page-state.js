'use strict';

app.factory('PageState', [function() {
	
	var Scope = {};

	var bind2Scope = function($scope) {
		$scope.PageState = {}
		Scope = $scope;
	}

	var getState = function() {
		return Scope.PageState;
	}

	var setState = function(val) {
		Scope.PageState = val;
	}


	return {
		'bind2Scope' : bind2Scope,
		'getState' : getState,
		'setState' : setState
	}

}]);
