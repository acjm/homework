var app = angular.module('hw2', ['CDELogin']);

app.constant('StateConst', {
	'PROFILE' : 'profile',
	'CREATE_WALK' : 'create-walk',
	'VIEW_PRIVATE_WALK' : 'view-private-walk',
	'VIEW_PUBLIC_WALK' : 'view-public-walk',
	'HOME' : 'home',
	'SEARCH' : 'search'
})
