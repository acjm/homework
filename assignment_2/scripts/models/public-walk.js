'use strict';

app.factory('PublicWalkModel', [function() {
	
	var Scope = {};

	var bind2Scope = function($scope) {

		$scope.PublicWalk = 
			{
				'id' : 12345,
				'title' : 'Amazing Walk 1',
				'joined' : false,
				'date' : 'April 1',
				'time' : '1 PM',
				'photos' : [
					'http://img.timeinc.net/time/photoessays/2008/trees/franklin_trees_01.jpg',
					'http://img.timeinc.net/time/photoessays/2008/trees/franklin_trees_01.jpg'
				],
				'likes' : 100,
				'tags' : [
					'Amazing',
					'Too Amazing'
				],
				'members' : [
					'Amazing Alvin',
					'Amazing Chris',
					'Amazing John',
					'Amazing Michael'
				],
				'comments' : [
					{
						'posted_by' : 'Amazing Michael',
						'comment' : 'I will join this amazing walk!'
					},
					{
						'posted_by' : 'Amazing Alvin',
						'comment' : 'Great see you there!'
					}
				]
			};

		Scope = $scope;
	}

	return {
		'bind2Scope' : bind2Scope
	} 

}]);
