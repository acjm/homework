'use strict';

app.factory('PersonalWalksModel', [function() {
	
	var Scope = {};

	var bind2Scope = function($scope) {

		$scope.PersonalWalksModel = [
			{
				'title' : 'Amazing Walk 1', 
				'creator' : 'Amazing Alvin',
				'is_shared' : false,
				'is_started' : false,
				'description' : 'This walk is really amazing, please take it!'
			},
			{ 
				'title' : 'Amazing Walk 2' ,
				'creator' : 'Amazing Chris',
				'is_shared' : false,
				'is_shared' : true,
				'is_started' : true,
				'description' : 'This walk is really amazing, please take it!'
			},
			{
				'title' : 'Amazing Walk 3', 
				'creator' : 'Amazing John',
				'is_shared' : false,
				'is_started' : false,
				'description' : 'This walk is really amazing, please take it!'
			},
			{
				'title' : 'Amazing Walk 4',
				'creator' : 'Amazing Michael',
				'is_shared' : true,
				'is_started' : false,
				'description' : 'This walk is really amazing, please take it!'
			}

		];

		Scope = $scope;
	}

	return {
		'bind2Scope' : bind2Scope
	} 

}]);
