'use strict';

app.factory('SearchModel', [function() {
	
	var Scope = {};

	var bind2Scope = function($scope) {

		$scope.Search;

		$scope.SearchModel = [
			{
				'title' : 'Amazing Walk 1', 
				'created_by' : 'Amazing Alvin',
				'description' : 'This walk is really amazing, please take it!'
			},
			{ 
				'title' : 'Amazing Walk 2' ,
				'created_by' : 'Amazing Chris',
				'description' : 'This walk is really amazing, please take it!'
			},
			{
				'title' : 'Amazing Walk 3', 
				'created_by' : 'Aamzing John',
				'description' : 'This walk is really amazing, please take it!'
			},
			{
				'title' : 'Amazing Walk 4',
				'created_by' : 'Amazing Michael',
				'description' : 'This walk is really amazing, please take it!'
			}

		];

		Scope = $scope;
	}

	return {
		'bind2Scope' : bind2Scope
	} 

}]);
