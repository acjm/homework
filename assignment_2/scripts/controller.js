'use strict';

app.controller('AppCtrl', [
	'$scope',
	'StateConst',
	'PageState',
	'SearchModel',
	'PersonalWalksModel',
	'PublicWalkModel',
function($scope, StateConst, PageState, SearchModel, PersonalWalksModel, PublicWalkModel) {

	(function() {
		$scope.StateConst = StateConst;

		PageState.bind2Scope($scope);

		PageState.setState(StateConst.HOME);

		SearchModel.bind2Scope($scope);
		PersonalWalksModel.bind2Scope($scope);
		PublicWalkModel.bind2Scope($scope);
	})();

	$scope.changePage = function(event, state) {
		event.preventDefault();

		PageState.setState(state);	
	}

	$scope.search = function(event, state) {
		event.preventDefault();
		debugger

		PageState.setState(state);	
	}

}]);
	

