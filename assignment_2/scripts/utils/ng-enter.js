'use strict';

app.directive('ngEnter', function ($parse) {

	return function (scope, element, attrs) {
	
		element.bind("keydown keypress", function (event) {

			if(event.which === 13) {

				scope.$apply(function (){

					var fn = $parse(attrs.ngEnter);
					fn(scope, {$event:event})
					
				});

				event.preventDefault();
			}

		});
	};

});
